//NO.1
console.log('NO. 1');
const golden = ["this", "is", "golden!!"];

golden.forEach(gold => {
  console.log(gold); // senin selasa rabu
});

//NO.2
console.log('NO. 2');
const newFunction = function literal(firstName, lastName) {
  return {
    firstName: firstName,
    lastName: lastName,
    fullName() {
      console.log(`${firstName} ${lastName}`)
      return 
    }
  }
};

newFunction("William", "Imoh").fullName() 

//NO.3
console.log('NO. 3');
const newObject = {
  firstName: "Harry",
  lastName: "Potter Holt",
  destination: "Hogwarts React Conf",
  occupation: "Deve-wizard Avocado",
  spell: "Vimulus Renderus!!!"
}
var firstName, lastName, destination, occupation, spell;
({firstName, lastName, destination, occupation}=newObject);

console.log(firstName, lastName, destination, occupation)

//NO.4
console.log('NO. 4');
const west = [
			  ["Will", "Chris", "Sam", "Holly"],
			  ["Gill", "Brian", "Noel", "Maggie"]
			 ];
const combined = [].concat(...west);
console.log(combined)

//NO.5
console.log('NO. 5');
const planet = "earth"
const view = "glass"
var before = 'Lorem ' + view + 'dolor sit amet, ' +  
    'consectetur adipiscing elit,' + planet + 'do eiusmod tempor ' +
    'incididunt ut labore et dolore magna aliqua. Ut enim' +
    ' ad minim veniam'
 
// Driver Code
console.log(`Lorem ${view} dolor sit amet, consectetur adipiscing elit, ${planet} do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam`) 
