// // NO. 1
console.log('NO. 1');
function arrayToObject(dateString) {
    var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
    var today = new Date();
    var birthDate = new Date(dateString);
    var age = today.getFullYear() - birthDate.getFullYear();
    var m = today.getMonth() - birthDate.getMonth();
    if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
        age--;
    }
    else{
    	console.log("invalid birthDay");
    }
    return age;
}
console.log('firstName: Bruce'+'lastName: Banner'+'gender: male'+'age: ' + arrayToObject(1975));
console.log('firstName: Natasha'+'lastName: Romanoff'+'gender: female'+'age: ' + arrayToObject());
console.log('firstName: Tony'+'lastName: Stark'+'gender: male'+'age: ' + arrayToObject(1980));
console.log('firstName: Pepper'+'lastName: Pots'+'gender: female'+'age: ' + arrayToObject(2023));