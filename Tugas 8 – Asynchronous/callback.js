function readBooks(time, book) {
    setTimeout(function(callback){
        let sisaWaktu=0
        if(time > book.timeSpent) {
            sisaWaktu = time - book.timeSpent
            console.log(`saya membaca ${book.name}`)
            console.log(`saya sudah membaca ${book.name}, sisa waktu saya ${sisaWaktu}`)
            return sisaWaktu
            callback(sisaWaktu-book.timeSpent)
        } else {
            console.log('waktu saya habis')
            callback(time)
        }  
    }, book.timeSpent-time)
}

module.exports = readBooks

